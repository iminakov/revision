package kz.ivan.interview;

import java.lang.annotation.*;

@Inherited
@Target({ElementType.TYPE, ElementType.METHOD, ElementType.CONSTRUCTOR, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface Description {

	String author();

	String description();
}
