package kz.ivan.interview.api;

import kz.ivan.interview.domain.Clazz;
import kz.ivan.interview.domain.Element;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface ElementRepo extends JpaRepository<Element, Long>, JpaSpecificationExecutor<Element> {
	@Query("select e from Element e where e.clazz = :clazz and e.name = :name")
	Element getByElement(@Param("clazz") Clazz clazz, @Param("name") String name);
}
