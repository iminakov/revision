package kz.ivan.interview.api;

import kz.ivan.interview.Description;
import kz.ivan.interview.JarLoader;
import kz.ivan.interview.domain.Clazz;
import kz.ivan.interview.domain.Element;
import kz.ivan.interview.domain.Revision;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.time.ZonedDateTime;
import java.util.*;

import static kz.ivan.interview.util.Utils.copy;

@RestController
@RequestMapping("/processorJar")
public class ClazzController {
	@Autowired
	private ClazzRepo clazzRepo;

	@Autowired
	private ElementRepo elementRepo;

	@Autowired
	private RevisionRepo revisionRepo;

	@RequestMapping(value = "", method = RequestMethod.GET)
	public List<Clazz> list() {
		return clazzRepo.findAll();
	}

	@RequestMapping(value = "", method = RequestMethod.POST)
	public void loadJar(MultipartHttpServletRequest request, HttpServletResponse response) {
		File file = null;
		Iterator<String> requestFileNames = request.getFileNames();
		while (requestFileNames.hasNext()) {
			MultipartFile multipartFile = request.getFile(requestFileNames.next());
			try {
				InputStream stream = multipartFile.getInputStream();
				file = File.createTempFile("analyze", ".jar");
				OutputStream os = new FileOutputStream(file);
				copy(stream, os);
				os.flush();
				os.close();
			} catch (IOException e) {
				e.printStackTrace();
				return;
			}
			JarLoader jarLoader = new JarLoader(file.getAbsolutePath());
			persistRevision(jarLoader.getClassInfoMap());
			file.delete();
		}
	}

	/**
	 * Сохранение данных в БД
	 *
	 * @param map
	 */
	private synchronized void persistRevision(Map<String, Map<String, Description>> map) {
		for (String clazzName : map.keySet()) {
			Clazz clazz = clazzRepo.findByName(clazzName);
			if (clazz == null) {
				clazz = new Clazz();
				clazz.setName(clazzName);
			}
			Set<Element> elementSet = new HashSet<>();
			for (Map<String, Description> elements : map.values()) {
				for (String elementName : elements.keySet()) {
					Element element = clazz.getId() != null ? elementRepo.getByElement(clazz, elementName) : null;
					Description description = elements.get(elementName);
					if (element == null) {
						element = new Element();
						element.setClazz(clazz);
						element.setName(elementName);
						Set<Revision> revisionSet = getRevisions(description, element);
						element.setRevisions(revisionSet);
						elementSet.add(element);
					} else {
						List<Revision> revisionList = revisionRepo.findByElementOrderByDateDesc(element);
						Revision revision = revisionList.get(0);//берем всегда первый объект, мы знаем что коллекция всегдаесть и в ней есть как минимум 1 объект
						if (!(revision.getAuthor().equals(description.author()))
							|| !(revision.getDescription().equals(description.description()))) {
							Set<Revision> revisionSet = getRevisions(description, element);
							element.setRevisions(revisionSet);
							elementSet.add(element);
						}
					}
				}
			}
			clazz.setElements(elementSet);
			clazzRepo.save(clazz);
		}
	}

	/**
	 * Создание ревизий
	 *
	 * @param description аннотация
	 * @param element     элемент
	 * @return Set<Revision>
	 */
	private Set<Revision> getRevisions(Description description, Element element) {
		Set<Revision> revisionSet = new HashSet<>();
		Revision revision = new Revision();
		revision.setElement(element);
		revision.setAuthor(description.author());
		revision.setDescription(description.description());
		revision.setDate(ZonedDateTime.now());
		revisionSet.add(revision);
		return revisionSet;
	}
}
