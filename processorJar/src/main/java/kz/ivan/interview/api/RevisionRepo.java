package kz.ivan.interview.api;

import kz.ivan.interview.domain.Element;
import kz.ivan.interview.domain.Revision;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface RevisionRepo extends JpaRepository<Revision, Long>, JpaSpecificationExecutor<Revision> {
	@Query("select r from Revision r where r.element = :element  order by r.date desc")
	List<Revision> findByElementOrderByDateDesc(@Param("element") Element element);
}