package kz.ivan.interview.api;

import kz.ivan.interview.domain.Clazz;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface ClazzRepo extends JpaRepository<Clazz, Long>, JpaSpecificationExecutor<Clazz> {
	Clazz findByName(String name);
}
