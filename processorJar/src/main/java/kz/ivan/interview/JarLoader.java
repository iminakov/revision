package kz.ivan.interview;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.Executable;
import java.lang.reflect.Field;
import java.lang.reflect.Parameter;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static kz.ivan.interview.util.Utils.copy;


public class JarLoader {
	public static final String CLASS_SUFFIX = ".class";
	private Map<String, Map<String, Description>> classInfoMap = new HashMap<>();
	private String jarFile;

	public JarLoader(String jarFile) {
		this.jarFile = jarFile;
		loadClasses();
	}

	/**
	 * Извлекаем все классы из jar
	 */
	private void loadClasses() {
		JarFile jarFile;
		URLClassLoader urlClassLoader;
		try {
			URL[] classLoaderUrls = new URL[]{new URL("file:" + this.jarFile)};
			urlClassLoader = new URLClassLoader(classLoaderUrls);
			File genericJarFile = new File(this.jarFile);
			jarFile = new JarFile(genericJarFile);
		} catch (IOException e) {
			e.printStackTrace();
			return;
		}
		Enumeration<JarEntry> jarEntries = jarFile.entries();
		while (jarEntries.hasMoreElements()) {
			JarEntry jarEntry = jarEntries.nextElement();
			if (jarEntry.isDirectory() || jarEntry.getName().contains("$")) {
				continue;
			}
			if (jarEntry.getName().endsWith(CLASS_SUFFIX)) {
				String className = jarEntry.getName().replace('/', '.').substring(0, jarEntry.getName().length() - CLASS_SUFFIX.length());
				try {
					Class clazz = urlClassLoader.loadClass(className);
					classInfoMap.put(clazz.getName(), loadClassInfo(clazz));
				} catch (ClassNotFoundException e) {
					e.printStackTrace();
				}
			}
		}
	}

	/**
	 * Загружаем елементы класса в map
	 *
	 * @param clazz - класс
	 * @return мапа
	 */
	private Map<String, Description> loadClassInfo(Class<?> clazz) {
		Map<String, Description> map = new HashMap<>();
		map.put("class", clazz.getAnnotation(Description.class)); //аннотация над классом
		map.putAll(process(clazz.getDeclaredConstructors(), this::process));//аннотации над конструкторами
		map.putAll(process(clazz.getDeclaredMethods(), this::process));//аннотации над методами
		map.putAll(process(clazz.getDeclaredFields(), Field::getName));//аннотации над полями
		Stream.of(clazz.getClasses()).forEach(cl -> map.putAll(loadClassInfo(cl)));//пробегаем по вложенным классам
		return map;
	}

	/**
	 * Создаем мапу елементтов (наименование елемента, (параметры))
	 *
	 * @param src
	 * @param name
	 * @param <T>
	 * @return
	 */
	private <T extends AnnotatedElement> Map<String, Description> process(T[] src, Function<T, String> name) {
		return Stream.of(src)
				.filter(v -> v.getAnnotation(Description.class) != null)
				.collect(Collectors.toMap(name, v -> v.getAnnotation(Description.class)));
	}

	/**
	 * Соираем параметры конструкторов,методов
	 *
	 * @param method
	 * @param <T>
	 * @return
	 */
	private <T extends Executable> String process(T method) {
		return method.getName() + Stream.of(method.getParameters())
				.map(Parameter::getParameterizedType)
				.map(Object::toString)
				.collect(Collectors.joining(", ", "(", ")"));
	}

	/**
	 * Получаем байт-код класса
	 *
	 * @param jarFile
	 * @param jarEntry
	 * @return
	 * @throws IOException
	 */
	private byte[] loadClassData(JarFile jarFile, JarEntry jarEntry) throws IOException {
		long size = jarEntry.getSize();
		if (size <= 0) {
			return null;
		} else if (size > Integer.MAX_VALUE) {
			throw new IOException("Class size too long");
		}

		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		copy(jarFile.getInputStream(jarEntry), outputStream);
		return outputStream.toByteArray();
	}

	public Map<String, Map<String, Description>> getClassInfoMap() {
		return classInfoMap;
	}
}


