package kz.ivan.interview.util;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class Utils {
	public static void copy(InputStream src, OutputStream dst) throws IOException {
		byte[] buffer = new byte[16];
		int len;
		while ((len = src.read(buffer)) > 0) {
			dst.write(buffer, 0, len);
		}
		src.close();
	}

}
