CREATE TABLE clazz (
		id   IDENTITY CONSTRAINT pk$clazz PRIMARY KEY,
		name VARCHAR(1024)
);
CREATE TABLE element (
		id       IDENTITY CONSTRAINT pk$element PRIMARY KEY,
		clazz_id BIGINT,
		name     VARCHAR(1024)
);

CREATE TABLE revision (
		id          IDENTITY CONSTRAINT pk$revision PRIMARY KEY,
		element_id  BIGINT,
		author      VARCHAR(255),
		description VARCHAR(1024),
		date        TIMESTAMP
);

