var app = angular.module('processorJar', ['ui.uploader']);

app.controller('Ctrl', function ($scope, $log, uiUploader, $http) {
	$http.get('processorJar').success(function (data) {
		$scope.clazz = data;
	});
	$scope.selectedClazz = undefined;

	$scope.selectClazz = function (item) {
		$scope.selectedClazz = item;
		$scope.selectedElement = undefined;
	};
	$scope.selectedElement = undefined;

	$scope.selectElement = function (item) {
		$scope.selectedElement = item;
	};
	$scope.sortType = 'date';
	$scope.sortReverse = false;
	$scope.searchAuthor = '';

	function getData() {
		$http.get('processorJar').success(function (data) {
			$scope.clazz = data;
		});
	}

	$scope.btn_remove = function (file) {
		$log.info('deleting=' + file);
		uiUploader.removeFile(file);
	};
	$scope.btn_clean = function () {
		uiUploader.removeAll();
	};
	$scope.btn_upload = function () {
		$log.info('uploading...');
		uiUploader.startUpload({
			url: 'http://localhost:8080/processorJar',
			concurrency: 2,
			onProgress: function (file) {
				$log.info(file.name + '=' + file.humanSize);
				$scope.$apply();
			},
			onCompleted: function (file, response) {
				$log.info(file + 'response' + response);
				getData();
				$scope.$apply();
			}
		});
	};
	$scope.files = [];
	var element = document.getElementById('file1');
	element.addEventListener('change', function (e) {
		var files = e.target.files;
		uiUploader.addFiles(files);
		$scope.files = uiUploader.getFiles();
		$scope.$apply();
	});
});

