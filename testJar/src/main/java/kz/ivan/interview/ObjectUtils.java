package kz.ivan.interview;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.beans.*;
import java.io.*;
import java.lang.reflect.*;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.*;
import java.util.*;

@SuppressWarnings({"UnusedDeclaration", "JavaDoc", "UnnecessaryUnboxing", "unchecked", "SimplifiableIfStatement"})
@Description(author = "Ivan.Minakov",
		description = "Вспомогательный клас")
public final class ObjectUtils {
	public static final int OBJECT_NOT_FOUND = -1;
	private static final Logger log = LoggerFactory.getLogger(ObjectUtils.class);

	@Description(author = "Ivan.Minakov",
			description = "Конструктор")
	private ObjectUtils() {
	}

	/**
	 * Null-safe equals
	 *
	 * @param o1
	 * @param o2
	 * @return Null-safe equals
	 */
	@Description(author = "Ivan.Minakov",
			description = "Null-safe equals")
	public static boolean equals(Object o1, Object o2) {
		if (o1 == null && o2 == null) {
			return true;
		}
		if (o1 != null && o2 != null) {
			return o1.equals(o2);
		}
		return false;
	}

	/**
	 * Null-safe compareTo
	 *
	 * @param o1
	 * @param o2
	 * @return Null-safe compareTo
	 */
	@Description(author = "Ivan.Minakov",
			description = "Null-safe compareTo")
	public static int compareTo(Comparable o1, Comparable o2) {
		if (o1 == null && o2 == null) {
			return 0;
		}
		if (o1 != null && o2 != null) {
			return o1.compareTo(o2);
		}
		return -1;
	}

	public static int compareToWithNull(Comparable o1, Comparable o2) {
		return o1 == null && o2 == null ? 0 : o1 == null ? 1 : o2 == null ? -1 : o1.compareTo(o2);
	}

	/**
	 * Null-safe intValue
	 *
	 * @param number
	 * @param defaultValue
	 * @return Null-safe intValue
	 */
	@Description(author = "Ivan.Minakov",
			description = "Null-safe intValue")
	public static int intValue(Number number, int defaultValue) {
		return number == null ? defaultValue : number.intValue();
	}

	/**
	 * Null-safe floatValue
	 *
	 * @param number
	 * @param defaultValue
	 * @return Null-safe floatValue
	 */
	@Description(author = "Ivan.Minakov",
			description = "Null-safe floatValue")
	public static float floatValue(Float number, float defaultValue) {
		return number == null ? defaultValue : number.floatValue();
	}

	/**
	 * Null-safe doublValue from Number
	 *
	 * @param value
	 * @return Null-safe doubleValue
	 */
	@Description(author = "Ivan.Minakov",
			description = "Null-safe doublValue from Number")
	public static boolean booleanValue(Boolean value) {
		return value != null && value;
	}

	@Description(author = "Ivan.Minakov",
			description = "Null-safe doublValue from Number")
	public static boolean booleanValue(Boolean value, boolean def) {
		return value != null ? value : def;
	}

	/**
	 * Null-safe doubleValue
	 *
	 * @param number
	 * @return Null-safe doubleValue
	 */
	@Description(author = "Ivan.Minakov",
			description = "Null-safe doubleValue")
	public static Double doubleValue(Number number) {
		return number == null ? null : number.doubleValue();
	}

	@Description(author = "Ivan.Minakov",
			description = "Null-safe doubleValue")
	public static Date dateValue(Timestamp dateTime) {
		return dateTime == null ? null : new Date(dateTime.getTime());
	}

	@Description(author = "Ivan.Minakov",
			description = "Number to Long")
	public static Long longValue(Number val) {
		return val != null ? val.longValue() : null;
	}

	@Description(author = "Ivan.Minakov",
			description = "Object to String")
	public static String toString(Object o) {
		return o != null ? o.toString() : null;
	}

	@Description(author = "Ivan.Minakov",
			description = "To empty String")
	public static String toEmptyString(Object o) {
		return emptyString(toString(o));
	}

	@Description(author = "Ivan.Minakov",
			description = "Empty string")
	public static String emptyString(String string) {
		return string != null ? string : "";
	}

	@Description(author = "Ivan.Minakov",
			description = "Format Time")
	public static final String formatTimeFormat = "y M d h m s ss";

	/**
	 * @param time Время, мс
	 * @return Форматирование времени в строку
	 */
	@Description(author = "Ivan.Minakov",
			description = "Long to String (Форматирование времени в строку)")
	public static String formatTime(Long time) {
		return formatTime(time, formatTimeFormat);
	}

	/**
	 * @param time Время
	 * @return Форматирование времени в строку
	 */
	@Description(author = "Ivan.Minakov",
			description = "Date to String (Форматирование времени в строку)")
	public static String formatTime(java.util.Date time) {
		return time != null ? new SimpleDateFormat("HH:mm").format(time) : "";
	}

	/**
	 * @param time Время, мс
	 * @param name Формат времени
	 * @return Форматирование времени в строку
	 */
	@Description(author = "Ivan.Minakov",
			description = "Форматирование времени в строку")
	public static String formatTime(Long time, String name) {
		Calendar col = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
		col.setTimeInMillis(time);

		String[] names = name.split(" ");

		//Получаем значения
		int[] values = new int[7];
		values[0] = col.get(Calendar.YEAR) - 1970;
		values[1] = col.get(Calendar.MONTH);
		values[2] = col.get(Calendar.DAY_OF_MONTH) - 1;
		values[3] = col.get(Calendar.HOUR_OF_DAY);
		values[4] = col.get(Calendar.MINUTE);
		values[5] = col.get(Calendar.SECOND);
		values[6] = col.get(Calendar.MILLISECOND);

		//Собираем в строку
		StringBuilder result = new StringBuilder();
		for (int i = 0; i < values.length; i++) {
			int value = values[i];
			if (value > 0) {
				result.append(value).append(names[i]).append(" ");
			}
		}

		return result.toString().trim();
	}

	/**
	 * @param date - can't be null
	 * @param from - if null then boundary check is true
	 * @param to   - if null then boundary checr is true
	 * @return true if date between from and to.
	 */
	@Description(author = "Ivan.Minakov",
			description = "true if date between from and to.")
	public static boolean dateBetween(java.util.Date date, java.util.Date from, java.util.Date to) {
		return (from == null || !date.before(from)) && (to == null || !date.after(to));
	}

	@Description(author = "Ivan.Minakov",
			description = "true if date between from and to.")
	public static boolean dateBetweenStandart(java.util.Date date) {
		try {
			java.util.Date from = parseDateStandart("01.01.1900");
			java.util.Date to = parseDateStandart("01.01.3000");

			return dateBetween(date, from, to);
		} catch (ParseException e) {
			log.warn("Invalid date", e);
			return false;
		}
	}

	@Description(author = "Ivan.Minakov", description = "yyyy-MM-dd")
	public static final DateFormat systemDateFormat = new SimpleDateFormat("yyyy-MM-dd");
	@Description(author = "Ivan.Minakov", description = "yyyy-MM-dd HH:mm:ss")
	public static final DateFormat systemDateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	@Description(author = "Ivan.Minakov", description = "dd.MM.yyyy")
	public static final DateFormat standartDateFormat = new SimpleDateFormat("dd.MM.yyyy");
	@Description(author = "Ivan.Minakov", description = "HH:mm")
	public static final DateFormat standartTimeFormat = new SimpleDateFormat("HH:mm");
	@Description(author = "Ivan.Minakov", description = "HH:mm:ss")
	public static final DateFormat standartTimeFormatWithSeconds = new SimpleDateFormat("HH:mm:ss");
	@Description(author = "Ivan.Minakov", description = "dd.MM.yyyy HH:mm:ss")
	public static final DateFormat standartDateTimeFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
	@Description(author = "Ivan.Minakov", description = "dd.MM.yyyy HH:mm")
	public static final DateFormat standartDateTimeFormatWithoutSeconds = new SimpleDateFormat("dd.MM.yyyy HH:mm");
	@Description(author = "Ivan.Minakov", description = "yy")
	public static final DateFormat yearFormat = new SimpleDateFormat("yy");
	@Description(author = "Ivan.Minakov", description = "yyyy")
	public static final DateFormat fullYearFormat = new SimpleDateFormat("yyyy");
	@Description(author = "Ivan.Minakov", description = "#0.0#########")
	private static final NumberFormat standartNumberFormat = new DecimalFormat("#0.0#########");

	/**
	 * @return формат "dd.MM.yyyy"
	 */
	@Description(author = "Ivan.Minakov", description = "формат \"dd.MM.yyyy\"")
	public static DateFormat getStandartDateFormat() {
		return standartDateFormat;
	}

	@Description(author = "Ivan.Minakov", description = "формат \"dd.MM.yyyy\"")
	public static String formatDateStandart(java.util.Date date) {
		return date != null ? getStandartDateFormat().format(date) : "";
	}

	public static String formatTimeWithSeconds(java.util.Date date) {
		return date != null ? standartTimeFormatWithSeconds.format(date) : "";
	}

	public static String formatTimeWithoutSeconds(java.util.Date date) {
		return date != null ? standartTimeFormat.format(date) : "";
	}

	public static String formatDateTimeStandart(java.util.Date date) {
		return date != null ? standartDateTimeFormatWithoutSeconds.format(date) : "";
	}

	public static String formatYear(java.util.Date date) {
		return date != null ? yearFormat.format(date) : "";
	}

	public static String formatYearFull(java.util.Date date) {
		return date != null ? fullYearFormat.format(date) : "";
	}

	public static DateFormat getStandartDateTimeFormatWithoutSeconds() {
		return standartDateTimeFormatWithoutSeconds;
	}

	public static java.util.Date parseDateStandart(String dateStr) throws ParseException {
		return getStandartDateFormat().parse(dateStr);
	}

	public static DateFormat getStandartTimeFormat() {
		return standartTimeFormat;
	}

	public static DateFormat getStandartTimeFormatWithSeconds() {
		return standartTimeFormatWithSeconds;
	}

	public static DateFormat getStandartDateTimeFormat() {
		return standartDateTimeFormat;
	}

	/**
	 * @return стандартный формат для чисел "#0.0#########"
	 */
	public static NumberFormat getStandartNumberFormat() {
		return standartNumberFormat;
	}

	public static Timestamp getNow() {
		return new Timestamp(new java.util.Date().getTime());
	}

	/**
	 * @return "обрезанную" сегодняшнюю дату
	 */
	public static Date getToday() {
		return Date.valueOf(systemDateFormat.format(new java.util.Date()));
	}

	/**
	 * @return "обрезанную" дату завтрашнего дня
	 */
	public static Date getTomorrow() {
		return addTime(Calendar.DATE, getToday(), 1);
	}


	/**
	 * @return date "обрезает" переданную дату
	 */
	public static java.util.Date truncDate(java.util.Date date) {
		if (date == null) {
			return null;
		}

		return Date.valueOf(systemDateFormat.format(date));
	}

	/**
	 * @return date "обрезает" переданное время
	 */
	public static java.util.Date truncTime(java.util.Date date) {
		if (date == null) {
			return null;
		}

		return new Date(date.getTime() - Date.valueOf(systemDateFormat.format(date)).getTime());
	}

	/**
	 * Метод обнуляет высокоточные поля даты, оставляя точность до дня.
	 * Может быть полезен в операциях сравнения (обычно требуется сравнение по дате,
	 * но java.util.Date хранит дату до миллисекунд, в БД по умолчанию дата до секунд)
	 *
	 * @param date исходное значение
	 * @return наименьшее значение даты в пределах дня, указанного в переданном параметре
	 */
	public static java.util.Date floorDate(java.util.Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.set(Calendar.MILLISECOND, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.HOUR, 0);
		cal.set(Calendar.AM_PM, 0);
		return cal.getTime();
	}

	/**
	 * Метод может быть удобен для получения значения, исопльзуемого в операции between,
	 * совместно с методом floorDate, при выборке по полям с точностью выше дня
	 *
	 * @param date
	 * @return наибольшее значение даты в пределах дня, указанного в переданном параметре
	 */
	public static java.util.Date ceilDate(java.util.Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.set(Calendar.AM_PM, 0);
		cal.set(Calendar.MILLISECOND, 999);
		cal.set(Calendar.SECOND, 59);
		cal.set(Calendar.MINUTE, 59);
		cal.set(Calendar.HOUR, 23);
		return cal.getTime();
	}

	/**
	 * Вставляет в строку заданный разделитель так, чтобы строки получились длины maxLength.
	 * Ну или немного больше:) так как сначала забивается следующее слово, а потом проверяется длина.
	 * Побочное действие - убирает повторяющиеся пробелы.
	 *
	 * @param source    исходная строка
	 * @param maxLength предпочтительная максимальная длина
	 * @param separator разделитель
	 * @return ту же строку с заданными разделителями
	 */
	public static String splitStringToLength(String source, int maxLength, String separator) {
		StringTokenizer tokenizer = new StringTokenizer(source, " ");
		StringBuilder buffer = new StringBuilder();
		while (tokenizer.hasMoreTokens()) {
			StringBuilder subBuffer = new StringBuilder();
			do {
				subBuffer.append(tokenizer.nextToken());
				subBuffer.append(" ");
			}
			while (subBuffer.length() < maxLength && tokenizer.hasMoreTokens());
			buffer.append(subBuffer.toString().trim());
			buffer.append(separator);
		}
		return buffer.toString().trim();
	}

	/**
	 * Дополняет полученную строку указанным количеством символов.
	 *
	 * @param toBeCompleted Строка, которую следует дополнить символами
	 * @param totalLength   Количество символов, которое следует добавить
	 * @param symbol        Символ, который следует добавить
	 * @param isSuffix      Если значение = true, добавляет символы в конце полученной строки. В противном случае - в начале (перед строкой)
	 * @return Строка, дополненная в начале или в конце указанным количеством символов
	 */
	public static String completeWithSymbol(String toBeCompleted, int totalLength, char symbol, boolean isSuffix) {
		if (toBeCompleted == null || toBeCompleted.length() >= totalLength) {
			return toBeCompleted;
		}

		int length = toBeCompleted.length();
		char[] chars = new char[totalLength];

		if (isSuffix) {
			toBeCompleted.getChars(0, length, chars, 0);
			Arrays.fill(chars, length, totalLength, symbol);
		} else {
			toBeCompleted.getChars(0, length, chars, totalLength - length);
			Arrays.fill(chars, 0, totalLength - length, symbol);
		}

		return String.valueOf(chars);
	}

	public static byte[] serializeToByteArray(Object obj) {
		ByteArrayOutputStream bout = new ByteArrayOutputStream();
		XMLEncoder encoder = new XMLEncoder(bout);
		encoder.setPersistenceDelegate(Date.class, new PersistenceDelegate() {
			protected Expression instantiate(Object oldInstance, Encoder out) {
				Date date = (Date) oldInstance;
				Long time = date.getTime();
				return new Expression(date, date.getClass(), "new", new Object[]{time});
			}
		});
		encoder.writeObject(obj);
		encoder.close();
		return bout.toByteArray();
	}

	public static Object deserializeFromByteArray(byte[] buf) {
		if (buf == null || buf.length == 0) {
			return null;
		}
		XMLDecoder decoder = new XMLDecoder(new ByteArrayInputStream(buf));
		Object result = decoder.readObject();
		decoder.close();
		return result;
	}

	/**
	 * Проверка объекта на пустоту.
	 * Сначала объект проверяется на null, а затем на "пустое" значение в зависимости от типа.
	 * Для чисел пустым значением является 0, для строки - "". Остальные объекты пустых значений не имеют.
	 * (если надо, можно дописать свой объект)
	 *
	 * @param o проверяемый объект
	 * @return Возвращает true, если объект пустой.
	 */
	public static boolean isEmpty(Object o) {
		if (o == null) {
			return true;
		}
		if (o instanceof String) {
			return "".equals(o);
		}
		if (o instanceof Number) {
			return 0 == ((Number) o).intValue();
		}
		if (o instanceof Collection) {
			return ((Collection) o).isEmpty();
		}
		if (o instanceof Map) {
			return ((Map) o).isEmpty();
		}
		if (o instanceof Iterable) {
			return !(((Iterable) o).iterator().hasNext());
		}
		if (o instanceof Object[]) {
			return ((Object[]) o).length == 0;
		}
		return false;
	}

	/**
	 * Обратно @see isEmpty
	 *
	 * @param o проверяемый объект
	 * @return Возвращает false, если объект пустой
	 */
	public static boolean isNotEmpty(Object o) {
		return !isEmpty(o);
	}

	/**
	 * Выставляет текущей дате, заданной в милисекундах, время в 00:00:00 (оставляет только дни, месяцы, годы)
	 *
	 * @param date
	 * @return новое значание даты
	 */
	public static long cutTime(long date) {
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(date);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		return cal.getTimeInMillis();
	}

	/**
	 * Выставляет текущей дате время в 00:00:00 и 1 января
	 *
	 * @param date дата
	 * @return новое значание даты
	 */
	public static java.util.Date getFirstDayOfYear(java.util.Date date) {
		Calendar calendar = GregorianCalendar.getInstance();
		calendar.setTime(floorDate(date));
		calendar.set(Calendar.MONTH, Calendar.JANUARY);
		calendar.set(Calendar.DAY_OF_MONTH, 1);
		return floorDate(calendar.getTime());
	}

	/**
	 * Выставляет текущей дате время в 00:00:00 и 1 число текущего месяца
	 *
	 * @param date дата
	 * @return новое значение даты
	 */
	public static java.util.Date getFirstDayOfMonth(java.util.Date date) {
		Calendar calendar = GregorianCalendar.getInstance();
		calendar.setTime(floorDate(date));
		calendar.set(Calendar.DAY_OF_MONTH, 1);
		return floorDate(calendar.getTime());
	}

	/**
	 * Выставляет текущей дате время в 29:59:59 и 31 декабря
	 *
	 * @param date дата
	 * @return новое значание даты
	 */
	public static java.util.Date getLastDayOfYear(java.util.Date date) {
		Calendar calendar = GregorianCalendar.getInstance();
		calendar.setTime(date);
		calendar.set(Calendar.MONTH, 11);
		calendar.set(Calendar.DAY_OF_MONTH, 31);
		return ceilDate(calendar.getTime());
	}

	/**
	 * Вытаскивает год из даты
	 *
	 * @param date дата
	 * @return год даты
	 */
	public static int getYear(java.util.Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		return calendar.get(Calendar.YEAR);
	}

	/**
	 * Выставляет текущей дате время в 00:00:00 (оставляет только дни, месяцы, годы)
	 *
	 * @param date
	 * @return новое значание даты
	 */
	public static Date cutTime(Date date) {
		return new Date(cutTime(date.getTime()));
	}

	/**
	 * Выставляет текущей дате время в 00:00:00 (оставляет только дни, месяцы, годы)
	 *
	 * @param date
	 * @return новое значание даты
	 */
	public static java.util.Date cutTime(java.util.Date date) {
		return new java.util.Date(cutTime(date.getTime()));
	}

	/**
	 * Возвращает количество дней между двумя датами заданными в милисекундах
	 *
	 * @param from начальная дата
	 * @param to   конечная дата
	 * @return количество дней
	 */
	public static int daysBeetwen(long from, long to) {
		return (int) Math.abs((cutTime(to) - cutTime(from)) / (24 * 60 * 60 * 1000));
	}

	/**
	 * Возвращает количество дней между двумя датами
	 *
	 * @param from начальная дата
	 * @param to   конечная дата
	 * @return количество дней
	 */
	public static int daysBeetwen(java.util.Date from, java.util.Date to) {
		return daysBeetwen(from.getTime(), to.getTime());
	}

	/**
	 * Возвращает количество дней между двумя датами
	 *
	 * @param from начальная дата
	 * @param to   конечная дата
	 * @return количество дней
	 */
	public static int daysBeetwen(Date from, Date to) {
		return daysBeetwen(from.getTime(), to.getTime());
	}

	/**
	 * Конвертирует миллисекунды в формат "N дней, X часов, Y минут, Z секунд"
	 *
	 * @param millis Период в миллисекундах
	 * @return Форматированная строка
	 */
	public static String convertMillisToDaysHoursMinutes(long millis, boolean withSeconds) {
		StringBuilder builder = new StringBuilder();
		millis = millis / 1000;

		int days = (int) (millis / (24 * 60 * 60));
		int hours = (int) ((millis - days * 24 * 60 * 60) / (60 * 60));
		int minutes = (int) ((millis - (days * 24 * 60 * 60 + hours * 60 * 60)) / 60);
		int secs = (int) (millis - (days * 24 * 60 * 60 + hours * 60 * 60 + minutes * 60));

		if (days > 0) {
			builder.append(days).append(" дней ");
		}
		if (hours > 0) {
			builder.append(hours).append(" часов ");
		}
		if (minutes > 0) {
			builder.append(minutes).append(" минут ");
		}
		if (secs > 0 && withSeconds) {
			builder.append(secs).append(" секунд ");
		}
		return builder.toString();
	}

	/**
	 * Глубокое копирование объекта.
	 *
	 * @param o Объект копирования
	 * @return копия объекта
	 */
	public static <T> T deepCopy(T o) {
		T newObj = null;

		try {
			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			ObjectOutputStream oos = new ObjectOutputStream(bos);
			oos.writeObject(o);
			ByteArrayInputStream bis = new ByteArrayInputStream(bos.toByteArray());
			ObjectInputStream ois = new ObjectInputStream(bis);

			newObj = (T) ois.readObject();

		} catch (IOException e) {
			log.warn("Catch IOException while deepCopy", e);
		} catch (ClassNotFoundException e) {
			log.warn("Catch invalid classes while deepCopy", e);
		}
		return newObj;
	}

	/**
	 * Объединение строковых представлений объектов в одну строку через разделитель
	 *
	 * @param delimiter разделитель
	 * @param objs      массив объектов
	 * @return
	 */
	public static String concatWDel(String delimiter, Object... objs) {
		if (delimiter == null) {
			throw new IllegalArgumentException("Delimiter cannot be null");
		}
		StringBuilder res = new StringBuilder();
		for (Object o : objs) {
			if (isNotEmpty(o)) {
				if (o instanceof Collection) {
					res.append(concatWDel(delimiter, ((Collection) o).toArray()));
				} else if (o instanceof Array) {
					res.append(concatWDel(delimiter, o));
				} else {
					if (toEmptyString(o).length() != 0) {
						res.append(o).append(delimiter);
					}
				}
			}
		}
		return res.length() > 0 ? res.substring(0, res.length() - delimiter.length()) : "";
	}

	public static String concatWNulls(String delimiter, String nullSymbol, Object... objs) {
		StringBuilder res = new StringBuilder();
		for (Object o : objs) {
			res.append(isEmpty(o) ? nullSymbol : o.toString()).append(delimiter);
		}
		return res.length() > 0 ? res.substring(0, res.length() - delimiter.length()) : "";
	}

	public static Date addDaysExcludingWeekends(Date receivedAt, int days) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(receivedAt);

		while (days > 0) {
			cal.add(Calendar.DATE, 1);
			if (cal.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY || cal.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
				continue;
			}

			days--;
		}

		return new Date(cal.getTime().getTime());
	}

	/**
	 * Функция возвращает дату, полученную прибавлением указанного количества указанного типа времени к заданной дате
	 *
	 * @param timeType Тип времени. Список констант находится в Calendar
	 * @param dateFrom Дата, являющейся исходной
	 * @param time     Количество времени
	 * @return Результат = исходная дата + время указанного типа
	 */
	public static Date addTime(int timeType, java.util.Date dateFrom, int time) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(dateFrom);
		cal.add(timeType, time);

		return new Date(cal.getTime().getTime());
	}

	public static <T> int getObjectPosition(List<T> list, T obj) {
		if (list == null) {
			return OBJECT_NOT_FOUND;
		}

		return getObjectPosition(list.toArray(), obj);
	}

	public static <T> int getObjectPosition(T[] array, T obj) {
		if (array == null) {
			return OBJECT_NOT_FOUND;
		}

		for (int objIdx = 0; objIdx < array.length; objIdx++) {
			T currObject = array[objIdx];
			if (equals(currObject, obj)) {
				return objIdx;
			}
		}

		return OBJECT_NOT_FOUND;
	}

	public static String getFileExtension(File file) {
		if (file == null || file.isDirectory()) {
			return null;
		}

		String fileName = file.getName();
		int lastDotPos = fileName.lastIndexOf('.');
		if (lastDotPos == -1 || lastDotPos + 1 >= fileName.length()) {
			return null;
		}

		return fileName.substring(lastDotPos + 1);
	}

	public static long secondsBetween(java.util.Date d1, java.util.Date d2) {
		return (d2.getTime() - d1.getTime()) / 1000;
	}

	public static boolean isFileOfGivenExtension(File file, String givenExtension) {
		if (isEmpty(givenExtension)) {
			return false;
		}

		String extension = getFileExtension(file);
		return isNotEmpty(extension) && givenExtension.equalsIgnoreCase(extension);
	}


	/**
	 * Null-safe ternary decision
	 *
	 * @param condition
	 * @param trueValue
	 * @param falseValue
	 * @return
	 */
	public static <T> T ternaryDecision(Boolean condition, T trueValue, T falseValue) {
		return condition == null ? null : condition ? trueValue : falseValue;
	}

	public static boolean in(Object value, Object... list) {
		return Arrays.asList(list).contains(value);
	}

	public static Date getDate(Timestamp dateTime) {
		return new Date(dateTime.getTime());
	}

	public static int parseInt(String val, int def) {
		return isEmpty(val) ? def : Integer.parseInt(val);
	}

	static final String[] bytesPostfix = new String[]{"B", "KiB", "MiB", "GiB", "TiB", "PiB", "EiB", "ZiB", "YiB"};

	public static String formatBytes(long b) {
		int memoryFactor = 0;
		double size = b;
		while (size > 1000.0) {
			memoryFactor++;
			size = (size / 1024);
		}
		String s;
		if (memoryFactor >= bytesPostfix.length) {
			s = "Unknown(" + memoryFactor + ")";
		} else {
			s = bytesPostfix[memoryFactor];
		}
		return String.format("%.2f %s", size, s);
	}

	/**
	 * декодирование строки
	 *
	 * @param str строка в ISO8859_1 кодировке
	 * @return обычная строка
	 */
	public static String decodeISO(String str) {
		if (str != null) {
			try {
				return new String(str.getBytes("ISO8859_1"), "UTF-8");
			} catch (Exception ignored) {
			}

		}
		return null;

	}

	public static String getMemoryInfo() {
		return String.format("Memory info. Max: %s; In use: %s; Free of in use: %s;"
				, formatBytes(Runtime.getRuntime().maxMemory())
				, formatBytes(Runtime.getRuntime().totalMemory())
				, formatBytes(Runtime.getRuntime().freeMemory())
		);//|############++++++________________________________|
	}

	public static Object detectDefaultValue(Class<?> type) {
		return type == null ? null
				: type.isAssignableFrom(String.class) ? ""
//                : type.isAssignableFrom(Short.class) ? Short.valueOf((short) 0)
//                : type.isAssignableFrom(Integer.class) ? 0
//                : type.isAssignableFrom(Long.class) ? 0L
				: type.isAssignableFrom(Boolean.class) ? false
				: null;
	}

	public static String getStrFIO(String surname, String name, String patronymic) {
		StringBuilder strFIO = new StringBuilder();
		if (isNotEmpty(name)) {
			strFIO.append(" ").append(name.substring(0, 1).toUpperCase()).append(".");
		}
		if (isNotEmpty(patronymic)) {
			strFIO.append(" ").append(patronymic.substring(0, 1).toUpperCase()).append(".");
		}
		if (isNotEmpty(surname)) {
			strFIO.append(surname);
		}
		return strFIO.toString().trim();
	}

	public static Method findMethodByName(Class<?> clazz, String name) {
		for (Method method : clazz.getMethods()) {
			if (method.getName().toLowerCase().equals(name.toLowerCase())) {
				return method;
			}
		}
		return null;
	}

	public static <T> Class<T> getGenericType(Class clazz, int pos) {
		ParameterizedType types = (ParameterizedType) clazz.getGenericSuperclass();
		Type type = types.getActualTypeArguments()[pos];
		if (type instanceof TypeVariable) {
			return (Class<T>) ((TypeVariable) type).getBounds()[0];
		} else {
			return (Class<T>) type;
		}
	}

}
